//
//  ViewController.swift
//  Weather
//
//  Created by Alexandr on 7/29/18.
//  Copyright © 2018 Alexandr. All rights reserved.
//

import UIKit
import StoreKit

final class MainViewController: UIViewController {

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var temperaturLabel: UILabel!
    @IBOutlet weak var typeDayLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var graphView: Graph!
    
    private let weatherService = WeatherService(yql: YQL())
    private var weather: Weather? = nil {
        didSet {
            if let weather = weather {
                UserDefaults.standard.set(try? PropertyListEncoder().encode(weather), forKey: "weather")
                view.layoutIfNeeded()
                cityLabel.text = weather.city
                placeLabel.text = "\(weather.region) / \(weather.country)"
                temperaturLabel.text = "\(String(describing: weather.condition.temperature)) ֯ "
                typeDayLabel.text = weather.condition.text
                collectionView.reloadData()
                let maxValues = weather.forecast.map({ $0.high })
                let minValues = weather.forecast.map({ $0.low })
                graphView.update(with: maxValues, minValues: minValues)
            }
        }
    }
    private var selectedIndex = 0 {
        willSet {
            collectionView.reloadData()
            collectionView.scrollToItem(at: IndexPath(item: newValue, section: 0), at: [.centeredVertically, .centeredHorizontally], animated: true)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewController()
        bindHandlers()
        weatherService.loadWeather(forCity: "le mans")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let data = UserDefaults.standard.value(forKey: "weather") as? Data {
            let weather = try? PropertyListDecoder().decode(Weather.self, from: data)
            self.weather = weather
        }
    }
    
    private func setupViewController() {
        graphView.tapHandler = { [weak self] index in
            guard let strongSelf = self else { return }
            strongSelf.selectedIndex = index
        }
    }
    
    private func bindHandlers() {
        weatherService.weatherHandler = { [weak self] weather in
            guard let strongSelf = self else { return }
            strongSelf.weather = weather
        }
    }

}

extension MainViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return weather?.forecast.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "day", for: indexPath) as! DayCollectionViewCell
        
        if let forecast = weather?.forecast {
            cell.maxMinLabel.text = "\(forecast[indexPath.item].high) ֯ / \(forecast[indexPath.item].low) ֯ "
            let calendar = Calendar.current
            let day = calendar.component(.day, from: forecast[indexPath.item].date)
            cell.dayLabel.text = "\(forecast[indexPath.item].day) \(day)"
            if selectedIndex == indexPath.item {
                cell.dayLabel.textColor = UIColor.blue
                cell.maxMinLabel.textColor = UIColor.blue
            } else {
                cell.dayLabel.textColor = UIColor.black
                cell.maxMinLabel.textColor = UIColor.black
            }
        }
        
        return cell
    }
    
}

