//
//  DateFormatter.swift
//  Weather
//
//  Created by Alexandr on 7/29/18.
//  Copyright © 2018 Alexandr. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    static func forecastWeather(string: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        return dateFormatter.date(from: string)
    }
    
}
