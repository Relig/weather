//
//  Weather.swift
//  Weather
//
//  Created by Alexandr on 7/29/18.
//  Copyright © 2018 Alexandr. All rights reserved.
//

import Foundation

final class ForecastItem: Codable {
    
    let date: Date
    let day: String
    let text: String
    let high: Int
    let low: Int
    
    init?(with dictionary: [String: Any]) {
        guard
            let dateString = dictionary["date"] as? String,
            let date = DateFormatter.forecastWeather(string: dateString),
            let text = dictionary["text"] as? String,
            let day = dictionary["day"] as? String,
            let highString = dictionary["high"] as? String,
            let lowString = dictionary["low"] as? String,
            let high = Int(highString),
            let low = Int(lowString)
        else {
            return nil
        }
        
        self.date = date
        self.text = text
        self.day = day
        self.high = high
        self.low = low
    }
}

final class Condition: Codable {
    
    let text: String
    let temperature: Int
    
    init?(with dictionary: [String: Any]) {
        guard
            let text = dictionary["text"] as? String,
            let temperatureString = dictionary["temp"] as? String,
            let temperature = Int(temperatureString)
            else {
                return nil
        }
        
        self.text = text
        self.temperature = temperature
    }
}

final class Weather: Codable {
    
    let forecast: [ForecastItem]
    let condition: Condition
    let city: String
    let country: String
    let region: String
    
    init?(with dictionary: [String: Any]) {
        guard
            let query = dictionary["query"] as? [String: Any],
            let results = query["results"] as? [String: Any],
            let channel = results["channel"] as? [String: Any],
            let item = channel["item"] as? [String: Any],
            let forecastDictionary = item["forecast"] as? [[String: Any]],
            let locationDictionary = channel["location"] as? [String: Any],
            let city = locationDictionary["city"] as? String,
            let country = locationDictionary["country"] as? String,
            let region = locationDictionary["region"] as? String,
            let conditionDictionary = item["condition"] as? [String: Any],
            let condition = Condition(with: conditionDictionary)
        else {
            return nil
        }
        var forecast: [ForecastItem] = []
        for item in forecastDictionary {
            if let condition = ForecastItem(with: item) {
                forecast.append(condition)
            }
        }
        self.forecast = forecast
        self.city = city
        self.country = country
        self.region = region
        self.condition = condition
    }
    
}
