//
//  WeatherService.swift
//  Weather
//
//  Created by Alexandr on 7/30/18.
//  Copyright © 2018 Alexandr. All rights reserved.
//

import Foundation

final class WeatherService {
    
    var weatherHandler: ((Weather) -> Void)?
    
    private let yql: YQL
    
    init(yql: YQL) {
        self.yql = yql
    }
    
    func loadWeather(forCity city: String) {
        let query = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"\(city)\") and u='c'"
        if let results = yql.query(query) as? [String: Any], let weather = Weather(with: results) {
            weatherHandler?(weather)
        }
    }
    
}
