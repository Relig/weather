//
//  DayCollectionViewCell.swift
//  Weather
//
//  Created by Alexandr on 7/29/18.
//  Copyright © 2018 Alexandr. All rights reserved.
//

import UIKit

final class DayCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var maxMinLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    
}
