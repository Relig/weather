//
//  Graph.swift
//  Weather
//
//  Created by Alexandr on 7/29/18.
//  Copyright © 2018 Alexandr. All rights reserved.
//

import UIKit

final class Graph: UIView {
    
    enum Constants {
        static let marginBetweenMultiplier: CGFloat = 3.0
        static let labelHeight: CGFloat = 12.0
        static let labelMargin: CGFloat = 6.0
    }
    
    var tapHandler: ((Int) -> Void)?
    
    private var labelsMax: [UILabel] = []
    private var layersMax: [CALayer] = []
    private var labelsMin: [UILabel] = []
    private var layersMin: [CALayer] = []
    
    func update(with maxValues: [Int], minValues: [Int]) {
        var maxValue = maxValues.max()!

        labelsMax.forEach({ $0.removeFromSuperview() })
        layersMax.forEach({ $0.removeFromSuperlayer() })
        labelsMax.removeAll()
        layersMax.removeAll()
        
        for value in maxValues {
            let layer = CALayer()
            layer.backgroundColor = UIColor.red.cgColor
            let lastLayer = layersMax.last
            let x = lastLayer == nil ? 0.0 : lastLayer!.frame.origin.x + lastLayer!.frame.width + lastLayer!.frame.width * Constants.marginBetweenMultiplier
            layer.frame = CGRect(x: x,
                                 y: bounds.height / 2.0,
                                 width: bounds.width / (CGFloat(maxValues.count) + CGFloat((maxValues.count - 1)) * Constants.marginBetweenMultiplier),
                                 height: -bounds.height / 2.0 * CGFloat(value) / CGFloat(maxValue) + Constants.labelHeight + Constants.labelMargin)
            layersMax.append(layer)
            self.layer.addSublayer(layer)
            
            let label = UILabel(frame: CGRect(x: layer.frame.origin.x - layer.frame.width,
                                              y: layer.frame.origin.y - Constants.labelHeight - Constants.labelMargin,
                                              width: layer.frame.width * Constants.marginBetweenMultiplier,
                                              height: Constants.labelHeight))
            label.text = "\(value)"
            label.textAlignment = .center
            label.font = label.font.withSize(11.0)
            labelsMax.append(label)
            addSubview(label)
        }
        
        maxValue = minValues.max()!
        
        labelsMin.forEach({ $0.removeFromSuperview() })
        layersMin.forEach({ $0.removeFromSuperlayer() })
        labelsMin.removeAll()
        layersMin.removeAll()
        
        for value in minValues {
            let layer = CALayer()
            layer.backgroundColor = UIColor.blue.cgColor
            let lastLayer = layersMin.last
            let x = lastLayer == nil ? 0.0 : lastLayer!.frame.origin.x + lastLayer!.frame.width + lastLayer!.frame.width * Constants.marginBetweenMultiplier
            layer.frame = CGRect(x: x,
                                 y: bounds.height / 2,
                                 width: bounds.width / (CGFloat(minValues.count) + CGFloat((minValues.count - 1)) * Constants.marginBetweenMultiplier),
                                 height: bounds.height / 2.0 * CGFloat(value) / CGFloat(maxValue) - Constants.labelHeight - Constants.labelMargin)
            layersMin.append(layer)
            self.layer.addSublayer(layer)
            
            let label = UILabel(frame: CGRect(x: layer.frame.origin.x - layer.frame.width,
                                              y: layer.frame.origin.y + layer.frame.height + Constants.labelMargin,
                                              width: layer.frame.width * Constants.marginBetweenMultiplier,
                                              height: Constants.labelHeight))
            label.text = "\(value)"
            label.textAlignment = .center
            label.font = label.font.withSize(11.0)
            labelsMin.append(label)
            addSubview(label)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let point = touch.location(in: self)
            let labelLayersMax = labelsMax.map({ $0.layer })
            let labelLayersMin = labelsMin.map({ $0.layer })
            let layers = [layersMax, layersMin, labelLayersMax, labelLayersMin]
            
            for array in layers {
                for (index, layer) in array.enumerated() {
                    if layer.frame.contains(point) {
                        tapHandler?(index)
                    }
                }
            }
        }
    }
    
}
